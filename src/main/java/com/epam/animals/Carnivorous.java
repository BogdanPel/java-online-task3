package com.epam.animals;

import com.epam.ecosystem.Eatable;

public interface Carnivorous {
    void eatAnimal(Animal animal);

}
