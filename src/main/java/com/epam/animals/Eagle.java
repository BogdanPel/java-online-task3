package com.epam.animals;

public class Eagle extends Bird  {
    public Eagle() {
        weight = 6;
    }

    @Override
    public void getTypeOfMovement() {
        super.getTypeOfMovement();
        System.out.println("Eagle can Fly and Walk as well");
    }
}
