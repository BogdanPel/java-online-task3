package com.epam.animals;

import com.epam.ecosystem.Eatable;
import com.epam.herbs.Herbs;

public class Bunny extends Animal implements Eatable, Herbivorous {

    private String name;

    public Bunny (String name) {
        this.name = name;
        System.out.println("Smiling,Im Bunny named " + this.name);
        weight = 3;
    }

    public String getName () {
        return  name;
    }

    private void eat(Herbs herb){
        if (!isAlive) throw new IllegalStateException("Dead Bunny doesn't eat");
        else {
            System.out.println("umnUmnMnom");
            gainWeight(herb.eaten());

        }
    }
    @Override
    protected void gainWeight(double calories) {
        weight += calories/300;


    }

    public double eaten() {
        this.die();
        return weight*50;
    }

    public void eatHerbs(Herbs herbs) {
        eat(herbs);
    }

    public void getTypeOfMovement() {
        System.out.println("Bunny can only Jump");
    }
}

