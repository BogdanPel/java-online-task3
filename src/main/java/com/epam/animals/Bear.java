package com.epam.animals;

import com.epam.herbs.Herbs;

public class Bear extends Animal implements Omnivorous {
    public Bear () {
        weight = 150;

    }

    protected void gainWeight(double calories) {
        weight+=calories/700;

    }

    public void eatAll(Animal animal) {
        animal.die();
        gainWeight(animal.eaten());

    }

    public void eatAll(Herbs herbs) {
      gainWeight(herbs.eaten());

    }


    public double eaten() {
        return weight*100;
    }

    public void getTypeOfMovement() {
        System.out.println("Bear can't Fly but Walk and Swim");

    }
}
