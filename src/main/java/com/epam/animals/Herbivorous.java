package com.epam.animals;

import com.epam.herbs.Herbs;

public interface Herbivorous {
    void eatHerbs(Herbs herbs);
}
