package com.epam.animals;

import com.epam.ecosystem.Eatable;

public abstract class Animal implements Eatable,Movement {
    protected boolean isSleeping ;
    protected double weight;
    protected boolean isAlive = true;

    public boolean isAlive() {
        return isAlive;
    }

    public boolean isSleeping() { return isSleeping; }

    public double getWeight() {
        return weight;
    }

    protected void die() {
        if(!isAlive || isSleeping) throw new IllegalStateException("This animal is already in Heaven or sleepin in a forest");
        isAlive = false;
    }

    protected abstract void gainWeight(double calories);

    public void setSleep(Animal animal) {
        if( animal instanceof Owl) return;
        animal.isSleeping = true;
        System.out.println("ZzzzzzzZZZZzzz");
    }
    public void  setAwake(Animal animal) {
        animal.isSleeping = false;
        System.out.println("Whooaa, I feels so rested");
    }


}

