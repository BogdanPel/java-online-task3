package com.epam.animals;
import com.epam.herbs.Herbs;

public interface Omnivorous  {
    void eatAll(Animal animal) ;
    void eatAll(Herbs herbs) ;
}

