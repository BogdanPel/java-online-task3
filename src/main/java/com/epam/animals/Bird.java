package com.epam.animals;

import com.epam.ecosystem.Eatable;
import com.epam.herbs.Herbs;

public  abstract class Bird extends Animal implements Eatable,Omnivorous {
    private String name ;
    public Bird() {
        weight = 2;
    }
   protected void gainWeight(double calories) {
        weight += calories/300;
   }

    public double eaten() {
        return weight*50;
    }


    public void eatAll(Animal animal) {
        animal.die();
        gainWeight(animal.eaten());
    }

    public void eatAll(Herbs herbs) {
        gainWeight(herbs.eaten());
    }

    public void getTypeOfMovement() {

    }
}
