package com.epam.animals;

public class Owl extends Bird {
 public Owl(){
     weight = 4.5;
    }

    @Override
    public void getTypeOfMovement() {
        super.getTypeOfMovement();
        System.out.println("Owl mostly sleeping but sometimes flying");
    }
}
