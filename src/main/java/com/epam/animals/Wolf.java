package com.epam.animals;


import com.epam.ecosystem.Eatable;

public class Wolf extends Animal implements Carnivorous  {
    public Wolf () {
        weight = 50;
        System.out.println("Woof,Woof,im Wolf");
    }

    public void eatBunny(Bunny bunny) {
        if(!bunny.isAlive()) {
            System.out.println("Nooo,someone already eated that bunny");
            return;
        }
        System.out.println(weight);
        gainWeight(bunny.eaten());


        System.out.println("Nyam,Nyam What a delicious " +  bunny.getName());
        System.out.println(weight);
    }

    @Override
    protected void gainWeight(double calories) {
        weight+=calories/100;
    }

    public void eatAnimal(Animal animal) {

        if (animal instanceof Bunny)
            eatBunny((Bunny)animal);

        animal.die();

        if (animal instanceof Eatable)
            gainWeight(((Eatable) animal).eaten());

    }

    public double eaten() {
        return 400;
    }

    public void getTypeOfMovement() {
        System.out.println("Wolf can Walk and Swim");
    }
}