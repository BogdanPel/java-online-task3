package com.epam.animals;

import com.epam.ecosystem.Eatable;
import com.epam.herbs.Herbs;

public class Moose extends Animal implements Eatable, Herbivorous {
    public Moose() {
        weight = 200;

    }
    public double eaten() {
        this.die();
        return weight*100;

    }
    @Override
    protected void gainWeight(double calories) {
        weight += calories*600;

    }

    public void eatHerbs(Herbs herbs) {
        this.gainWeight(herbs.eaten());
    }

    public void getTypeOfMovement() {
        System.out.println("Moose can Walk and Swim");
    }
}
