package com.epam.animals;

public class Parrot extends Bird {
    public Parrot() {
        weight = 2;
    }
    public void talk (String speech) {
        System.out.println("Parrot saying : " + speech);
    }

    @Override
    public void getTypeOfMovement() {
        System.out.println("Parrot can Fly and Walk");
    }
}
