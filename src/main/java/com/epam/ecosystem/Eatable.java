package com.epam.ecosystem;

public interface Eatable {
     double eaten();
}
