package com.epam.ecosystem;

import com.epam.animals.*;
import com.epam.herbs.Herbs;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

public class Ecosystem {
    public static void main(String[] args) {
        Wolf wolf = new Wolf();
        Bunny firstBunny = new Bunny("Piter");
        wolf.eatBunny(firstBunny);
        Bunny secondBunny = new Bunny("Rot");
        secondBunny.eatHerbs(new Herbs());
        secondBunny.eatHerbs(new Herbs());
        wolf.eatBunny(secondBunny);

//        ArrayList<Eatable> forest = new ArrayList<Eatable>();
//
//        forest.add(new Bunny("Oleg"));
//        forest.add(new Bunny("Olga"));
//        forest.add(new Moose());
//        forest.add(new Herbs());
//        forest.add(new Herbs());
//        forest.add(new Herbs());
//        forest.add(new Herbs());
//
//        Collections.shuffle(forest);
//
//        for (Eatable eatable: forest) {
//            if (eatable instanceof Animal){
//                wolf.eatAnimal((Animal) eatable);
//            }
//        }
         Moose moose = new Moose();
        System.out.println(moose.isSleeping());
        Bunny thirdBunny = new Bunny("Ilona");
        Parrot tinki = new Parrot();
        tinki.eatAll(new Herbs());
    }
}
